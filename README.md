# AI 测试用例提取

![应用预览截图](demo.png)

## 项目描述

一个使用 Nuxt 3、Vue 3 和 Tailwind CSS 构建的现代 AI 聊天应用，集成了 SiliconFlow AI API。

## 功能特点

- 实时 AI 聊天界面
- 现代化响应式设计
- 服务器端 API 处理
- 环境变量支持
- Tailwind CSS 样式

## 技术栈

- Nuxt 3
- Vue 3
- Tailwind CSS
- TypeScript
- SiliconFlow AI API

## 环境要求

- Node.js (v16 或更高版本)
- npm 或 yarn
- SiliconFlow API 密钥

## 安装步骤

1. 克隆仓库
