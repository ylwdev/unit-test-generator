interface Message {
  role: 'user' | 'assistant' | 'system';
  content: string;
  attachment?: string;
}

export const chatWithAI = async (
  messages: Message[],  // 改为接收完整的消息历史
  model: string = 'deepseek-ai/DeepSeek-V3'
): Promise<string> => {
  try {
    console.log('Sending to server:', {
      messages,
      model: model
    })

    const response = await fetch('/api/chat', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ 
        messages,  // 发送完整的消息历史
        model: model
      })
    });

    if (!response.ok) {
      throw new Error('API request failed');
    }

    return await response.text();
  } catch (error) {
    console.error('Chat error:', error);
    throw error;
  }
} 