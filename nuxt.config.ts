// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  app: {
    head: {
      title: 'AI测试用例分析',
      meta: [
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        { hid: 'description', name: 'description', content: 'AI驱动的测试用例分析工具' }
      ]
    }
  },
  css: [
    '~/assets/css/main.css',
    'highlight.js/styles/github.css'
  ],

  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  devtools: { enabled: true },
  compatibilityDate: '2025-02-07',
  nitro: {
    externals: {
      inline: ['marked', 'highlight.js']
    }
  }
})